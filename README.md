# applaudo-first-challenge

<img src="img/1.png" alt="landing-page">
<img src="img/2.png" alt="tasklist">
<img src="img/3.png" alt="calculator">

## About this repository

Little application that includes a tasklist and an aguinaldo calculator, developed for practice purposes. No important instructions required for running it, just open it in your favorite browser, the data is saved on the local storage of your browser (Light mode and dark mode too).
