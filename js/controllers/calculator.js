document.addEventListener('DOMContentLoaded', () => {
  clearForms();
});

document.querySelector('#calculator-form').addEventListener('submit', (e) => {
  e.preventDefault();

  const form = document.getElementById('calculator-form');
  const elements = form.querySelectorAll("input[type='text']");
  const errors = form.querySelectorAll('label.error');

  let warning = false;
  let message = '';
  elements.forEach((element) => {
    if (element.tagName == 'INPUT') {
      if (element.value.length == 0) {
        warning = true;
        message = 'Empty fields';
      }
    } else if (element.tagName == 'SELECT') {
      if (element.value == 'Choose someone') {
        warning = true;
        message = 'Unselected fields';
      }
    }
  });

  if (errors.length != 0) {
    warning = true;
    message = 'Invalid data, correct the fields';
  }

  if (!warning) {
    form.querySelector("button[type='submit']").disabled = true;

    let years = parseInt(form.querySelector('#txtYears').value);

    if (years == 0) {
      calculatorManagement.lessThanOneYear(form);
    } else if (years >= 1 && years <= 3) {
      calculatorManagement.betweenOneYearAndThreeYears(form);
    } else if (years >= 3 && years <= 10) {
      calculatorManagement.betweenThreeYearsAndTenYears(form);
    } else {
      calculatorManagement.greaterThanTenYears(form);
    }
  } else {
    Swal.fire('Warning', message, 'warning');
  }
});

const calculatorManagement = {
  lessThanOneYear: (form) => {
    let salary = parseFloat(form.querySelector('#txtSalary').value.replace(/[^\d\.]/g, '')).toFixed(2);
    let daysOfWork = parseInt(form.querySelector('#txtDays').value);
    let salaryDivided = (salary * 15) / 30;

    let bonus = (daysOfWork * salaryDivided) / 365;

    let dollarFormat = Intl.NumberFormat('en-US');

    document.getElementById('lblFinalAmount').textContent = `$${dollarFormat.format((parseFloat(bonus) + parseFloat(salary)).toFixed(2))}`;
    document.getElementById('lblBonus').textContent = `$${dollarFormat.format(bonus.toFixed(2))}`;
    document.getElementById('lblSalary').textContent = `$${dollarFormat.format(parseFloat(salary).toFixed(2))}`;

    form.querySelector("button[type='submit']").disabled = false;
  },
  betweenOneYearAndThreeYears: (form) => {
    let salary = parseFloat(form.querySelector('#txtSalary').value.replace(/[^\d\.]/g, '')).toFixed(2);
    let bonus = (salary * 15) / 30;

    let dollarFormat = Intl.NumberFormat('en-US');

    document.getElementById('lblFinalAmount').textContent = `$${dollarFormat.format((parseFloat(bonus) + parseFloat(salary)).toFixed(2))}`;
    document.getElementById('lblBonus').textContent = `$${dollarFormat.format(bonus.toFixed(2))}`;
    document.getElementById('lblSalary').textContent = `$${dollarFormat.format(parseFloat(salary).toFixed(2))}`;

    form.querySelector("button[type='submit']").disabled = false;
  },
  betweenThreeYearsAndTenYears: (form) => {
    let salary = parseFloat(form.querySelector('#txtSalary').value.replace(/[^\d\.]/g, '')).toFixed(2);
    let bonus = (salary * 19) / 30;

    let dollarFormat = Intl.NumberFormat('en-US');

    document.getElementById('lblFinalAmount').textContent = `$${dollarFormat.format((parseFloat(bonus) + parseFloat(salary)).toFixed(2))}`;
    document.getElementById('lblBonus').textContent = `$${dollarFormat.format(bonus.toFixed(2))}`;
    document.getElementById('lblSalary').textContent = `$${dollarFormat.format(parseFloat(salary).toFixed(2))}`;

    form.querySelector("button[type='submit']").disabled = false;
  },
  greaterThanTenYears: (form) => {
    let salary = parseFloat(form.querySelector('#txtSalary').value.replace(/[^\d\.]/g, '')).toFixed(2);
    let bonus = (salary * 21) / 30;

    let dollarFormat = Intl.NumberFormat('en-US');

    document.getElementById('lblFinalAmount').textContent = `$${dollarFormat.format((parseFloat(bonus) + parseFloat(salary)).toFixed(2))}`;
    document.getElementById('lblBonus').textContent = `$${dollarFormat.format(bonus.toFixed(2))}`;
    document.getElementById('lblSalary').textContent = `$${dollarFormat.format(parseFloat(salary).toFixed(2))}`;

    form.querySelector("button[type='submit']").disabled = false;
  },
};

function clearForms() {
  const forms = document.querySelectorAll('form');
  const inputs = document.querySelectorAll('input');
  const errors = document.querySelectorAll('label.error');

  forms.forEach((element) => {
    element.reset();
  });

  inputs.forEach((element) => {
    element.style.border = '1px solid transparent';
  });

  errors.forEach((element) => {
    element.remove();
  });

  document.getElementById('lblFinalAmount').textContent = '$0';
  document.getElementById('lblBonus').textContent = '$0';
  document.getElementById('lblSalary').textContent = '$0';
}

function validateIntegers(id, min, max, error) {
  const input = document.getElementById(id);
  if (input.value.match(/^\d+$/) && input.value >= min && input.value <= max) {
    input.style.border = '1px solid green';

    const errorLabel = document.querySelector(`#${input.id}Error`);
    if (errorLabel !== null) {
      errorLabel.remove();
    }
  } else {
    input.style.border = '1px solid red';

    if (document.querySelector(`#${input.id}Error`) === null) {
      const errorLabel = document.createElement('label');
      errorLabel.textContent = error;
      errorLabel.className = 'error ps-5';
      errorLabel.id = `${input.id}Error`;
      input.parentNode.parentNode.appendChild(errorLabel);
    }
  }
}

function validateCurrency(id, error) {
  const input = document.getElementById(id);
  if (input.value.match(/(?=.*\d)^\$?(([1-9]\d{0,2}(,\d{3})*)|0)?(\.\d{1,2})?$/)) {
    input.style.border = '1px solid green';

    const errorLabel = document.querySelector(`#${input.id}Error`);
    if (errorLabel !== null) {
      errorLabel.remove();
    }
  } else {
    input.style.border = '1px solid red';

    if (document.querySelector(`#${input.id}Error`) === null) {
      const errorLabel = document.createElement('label');
      errorLabel.textContent = error;
      errorLabel.className = 'error ps-5';
      errorLabel.id = `${input.id}Error`;
      input.parentNode.parentNode.appendChild(errorLabel);
    }
  }
}
