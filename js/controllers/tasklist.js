import TaskList from '../services/tasklist.js';
import TaskListUI from '../views/tasklist-ui.js';
import Validator from '../helpers/validator.js';
import LocalStorage from '../helpers/storage.js';

const tasksManagement = new TaskListUI();
const validator = new Validator();
const service = new TaskList();
const storage = new LocalStorage();

document.addEventListener('DOMContentLoaded', () => {
  if (storage.getItem('tasks') != null) {
    service.loadTasks(storage.getItem('tasks').reverse());
  }
  validator.blockFutureDates(document.querySelector("input[type='date']").id);
  validator.clearForms();
});

document.querySelector('#addtask-form').addEventListener('submit', (e) => {
  e.preventDefault();
  tasksManagement.saveTask('addtask-form');
});

document.querySelector('#editTask-form').addEventListener('submit', (e) => {
  e.preventDefault();
  tasksManagement.saveTask('editTask-form');
});

document.querySelector('#searchName-form').addEventListener('submit', (e) => {
  e.preventDefault();
  tasksManagement.filterByName('txtSearchName');
});

document.querySelector('#txtDate').addEventListener('change', () => {
  tasksManagement.filterByDate('txtDate');
});

document.querySelector('#cbStatus').addEventListener('change', () => {
  tasksManagement.filterByStatus('cbStatus');
});

document.querySelectorAll("input[type='text'], input[maxlength='100']").forEach((element) => {
  if (!element.hasAttribute('readonly')) {
    element.addEventListener('blur', (e) => {
      const id = e.currentTarget.id;
      const message = e.currentTarget.getAttribute('message');

      validator.validateAlphabetic(id, message);
    });
  }
});

document.addEventListener('click', (e) => {
  if (e.target.classList.contains('btn-table') || e.target.classList.contains('bi-pencil-fill')) {
    tasksManagement.loadTask(e.target.id);
  } else if (e.target.classList.contains('btn-table-2') || e.target.classList.contains('bi-trash-fill')) {
    tasksManagement.deleteTask(e.target.id);
  } else if (e.target.classList.contains('btn-table-3') || e.target.classList.contains('bi-arrow-clockwise')) {
    service.loadTasks(storage.getItem('tasks').reverse());
    validator.clearForms();
  } else if (e.target.classList.contains('pending-status') || e.target.classList.contains('done-status')) {
    tasksManagement.changeStatus(e.target.id);
  } else if (e.target.id == 'btnCreationDate' || e.target.id == 'btnDateIcon') {
    tasksManagement.sortByDate('btnDateIcon');
  } else if (e.target.classList.contains('btn-outline-success')) {
    tasksManagement.checkCheckboxValue(e.target.parentElement.id);
  }
});

document.querySelector('.modal-footer > .btn-primary').addEventListener('click', () => {
  document.querySelector("#editTask-form > button[type='submit']").click();
});
