export default class LocalStorage {
  getItem(key) {
    return JSON.parse(localStorage.getItem(key));
  }

  saveItem(key, data) {
    localStorage.setItem(key, JSON.stringify(data));
  }

  deleteItem(key, id) {
    let tasks = this.getItem(key);
    let index = 0;

    for (let i = 0; i < tasks.length; i++) {
      if (tasks[i].id == id) {
        index = i;
      }
    }

    let tempId = tasks[index].id;
    tasks.splice(index, 1);

    localStorage.setItem('tasks', JSON.stringify(tasks));

    return tempId;
  }
}
