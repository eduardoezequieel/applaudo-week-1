export default class Validator {
  validateAlphabetic(id, error) {
    const input = document.getElementById(id);
    if (input.value.match(/^[A-Z a-z 1-9]+$/)) {
      input.style.border = '1px solid green';

      const errorLabel = document.querySelector(`#${input.id}Error`);
      if (errorLabel !== null) {
        errorLabel.remove();
      }
    } else {
      input.style.border = '1px solid red';

      if (document.querySelector(`#${input.id}Error`) === null) {
        const errorLabel = document.createElement('label');
        errorLabel.textContent = error;
        errorLabel.className = 'error';
        errorLabel.id = `${input.id}Error`;
        input.parentNode.appendChild(errorLabel);
      }
    }
  }

  blockFutureDates(id) {
    let today = new Date();

    let day = ('0' + today.getDate()).slice(-2);
    var month = ('0' + (today.getMonth() + 1)).slice(-2);
    let year = today.getFullYear();

    let date = `${year}-${month}-${day}`;

    document.getElementById(id).setAttribute('max', date);
  }

  clearForms() {
    const forms = document.querySelectorAll('form');
    const inputs = document.querySelectorAll('input');
    const errors = document.querySelectorAll('label.error');

    forms.forEach((element) => {
      element.reset();
    });

    inputs.forEach((element) => {
      element.style.border = '1px solid transparent';
    });

    errors.forEach((element) => {
      element.remove();
    });
  }
}
