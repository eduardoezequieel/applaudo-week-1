class Colors {
  changeMode = () => {
    let colorMode = localStorage.getItem('colorMode');

    if (colorMode != null) {
      if (colorMode == 'dark') {
        this.darkMode();

        const wave = document.getElementById('wave');
        if (wave != null) {
          wave.setAttribute('fill', '#1b3450');
        }

        const btn = document.querySelector("button[tool='true'], a[tool='true']");
        if (btn != null) {
          btn.innerHTML = '<i class="bi bi-sun-fill"></i>';
        }
      } else if (colorMode == 'light') {
        this.lightMode();

        const wave = document.getElementById('wave');
        if (wave != null) {
          wave.setAttribute('fill', '#E9EDF2');
        }

        const btn = document.querySelector("button[tool='true'], a[tool='true']");
        if (btn != null) {
          btn.innerHTML = '<i class="bi bi-moon-fill"></i>';
        }
      }
    } else {
      localStorage.setItem('colorMode', 'light');
      this.changeMode();
    }
  };

  darkMode = () => {
    document.documentElement.style.setProperty('--background', '#011e3d');
    document.documentElement.style.setProperty('--background-hover', '#5f88b4');
    document.documentElement.style.setProperty('--text', 'white');
    document.documentElement.style.setProperty('--blue-color', '#017ffe');
    document.documentElement.style.setProperty('--blue-color-hover', '#b9dcff');
    document.documentElement.style.setProperty('--red-color', '#e35664');
    document.documentElement.style.setProperty('--red-color-hover', '#ffc6cc');
    document.documentElement.style.setProperty('--green-color', '#198754');
    document.documentElement.style.setProperty('--green-color-hover', '#a1e0bb');
    document.documentElement.style.setProperty('--green-2-color', '#20cb66');
    document.documentElement.style.setProperty('--yellow-color', '#cca445');
    document.documentElement.style.setProperty('--input-background', '#1b3450');
    document.documentElement.style.setProperty('--button-text', '#ffffff');
    document.documentElement.style.setProperty('--tr-line', '#011e3d');
    document.documentElement.style.setProperty('--text-mutted', '#C6C6C6');
  };

  lightMode = () => {
    document.documentElement.style.setProperty('--background', '#F2F7FA');
    document.documentElement.style.setProperty('--background-hover', '#000000');
    document.documentElement.style.setProperty('--text', 'black');
    document.documentElement.style.setProperty('--blue-color', '#017ffe');
    document.documentElement.style.setProperty('--blue-color-hover', '#b9dcff');
    document.documentElement.style.setProperty('--red-color', '#e35664');
    document.documentElement.style.setProperty('--red-color-hover', '#ffc6cc');
    document.documentElement.style.setProperty('--green-color', '#198754');
    document.documentElement.style.setProperty('--green-color-hover', '#a1e0bb');
    document.documentElement.style.setProperty('--green-2-color', '#20cb66');
    document.documentElement.style.setProperty('--yellow-color', '#cca445');
    document.documentElement.style.setProperty('--input-background', '#E9EDF2');
    document.documentElement.style.setProperty('--button-text', '#ffffff');
    document.documentElement.style.setProperty('--tr-line', '#C6C6C6');
    document.documentElement.style.setProperty('--text-mutted', '#6C757D');
  };
}

const colors = new Colors();

document.addEventListener('DOMContentLoaded', () => {
  colors.changeMode();
});

document.querySelector("button[tool='true'], a[tool='true']").addEventListener('click', () => {
  const mode = localStorage.getItem('colorMode');

  if (mode == 'dark') {
    localStorage.setItem('colorMode', 'light');
  } else {
    localStorage.setItem('colorMode', 'dark');
  }

  const style = document.createElement('style');
  document.head.appendChild(style);

  let rules = `
    * {
        transition: all 0.2s !important;
    }
  `;

  document.querySelector('style').innerHTML = rules;

  colors.changeMode();
});
