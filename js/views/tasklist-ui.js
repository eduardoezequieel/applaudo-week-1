import LocalStorage from '../helpers/storage.js';
import Validator from '../helpers/validator.js';
import TaskList from '../services/tasklist.js';

export default class TaskListUI {
  service = new TaskList();
  storage = new LocalStorage();
  validator = new Validator();

  saveTask(id) {
    const form = document.getElementById(id);
    const elements = form.querySelectorAll("input[type='text'], select");
    const errors = form.querySelectorAll('label.error');

    let warning = false;
    let message = '';
    elements.forEach((element) => {
      if (element.tagName == 'INPUT') {
        if (element.value.length == 0) {
          warning = true;
          message = 'Empty fields';
        }
      } else if (element.tagName == 'SELECT') {
        if (element.value == 'Choose someone') {
          warning = true;
          message = 'Unselected fields';
        }
      }
    });

    if (form.id == 'editTask-form') {
      if (form.querySelector("#checkboxGroup-1 > input[checked='true']") === null) {
        warning = true;
        message = 'Unchecked fields';
      }
    }

    if (errors.length != 0) {
      warning = true;
      message = 'Invalid data, correct the fields';
    }

    if (!warning) {
      form.querySelector("button[type='submit']").disabled = true;

      if (form.id == 'addtask-form') {
        let generatedId = this.service.addTask();
        this.validator.clearForms();

        Swal.fire('Success', 'Task added successfully', 'success').then(() => {
          this.service.loadTasks(this.storage.getItem('tasks').reverse());

          setTimeout(() => {
            document.getElementById(`row-${generatedId}`).className = 'animate__animated animate__flash';
          }, '1000');

          form.querySelector("button[type='submit']").disabled = false;
        });
      } else {
        this.service.editTask();

        Swal.fire('Success', 'Task edited successfully', 'success').then(() => {
          this.service.loadTasks(this.storage.getItem('tasks').reverse());
          setTimeout(() => {
            document.getElementById(`row-${document.getElementById('txtId').value}`).className = 'animate__animated animate__flash';
          }, '1000');
        });

        form.querySelector("button[type='submit']").disabled = false;
      }
    } else {
      Swal.fire('Warning', message, 'warning');
    }
  }

  loadTask(id) {
    document.getElementById('btnradio1').removeAttribute('checked');
    document.getElementById('btnradio2').removeAttribute('checked');

    let task = this.service.getTask(id);

    document.getElementById('txtId').value = task.id;
    document.getElementById('txtName2').value = task.name;
    document.getElementById('cbAssignee2').value = task.assignee;
    document.getElementById('lblCreationDate').textContent = task.creationDate;

    if (task.status == 'Pending') {
      document.getElementById('btnradio1').setAttribute('checked', 'true');
    } else {
      document.getElementById('btnradio2').setAttribute('checked', 'true');
    }
  }

  deleteTask(id) {
    Swal.fire({
      title: 'Delete',
      text: 'Are you sure that you want to delete this task?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#017ffe',
      cancelButtonColor: '#e35664',
      confirmButtonText: 'Yes, delete it.',
    }).then((result) => {
      if (result.isConfirmed) {
        this.service.deleteTask(id);

        Swal.fire('Success', 'Task deleted successfully', 'success').then(() => {
          document.getElementById(`row-${id}`).className = 'animate__animated animate__fadeOut';
          setTimeout(() => {
            this.service.loadTasks(this.storage.getItem('tasks').reverse());
          }, '1000');
        });
      }
    });
  }

  changeStatus(id) {
    Swal.fire({
      title: 'Warning',
      text: 'Are you sure that you want to change the status of this task?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#017ffe',
      cancelButtonColor: '#e35664',
      confirmButtonText: 'Yes, change it.',
    }).then((result) => {
      if (result.isConfirmed) {
        this.service.changeStatus(id);

        Swal.fire('Success', 'Status updated successfully', 'success').then(() => {
          this.service.loadTasks(this.storage.getItem('tasks').reverse());

          setTimeout(() => {
            document.getElementById(`row-${id}`).className = 'animate__animated animate__flash';
          }, '1000');
        });
      }
    });
  }

  filterByDate(id) {
    if (document.getElementById(id).value.length != 0) {
      let tasksOrderedByDate = this.service.filterByDate(id);
      if (tasksOrderedByDate.length != 0) {
        Swal.fire('Success', `${tasksOrderedByDate.length} tasks found`, 'success').then(() => {
          this.service.loadTasks(tasksOrderedByDate.reverse());
        });
      } else {
        Swal.fire('Message', 'No data on this date', 'warning');
      }
    }
  }

  filterByStatus(id) {
    if (document.getElementById(id).value != 'Choose a status') {
      let tasksOrderedByStatus = this.service.filterByStatus(id);

      if (tasksOrderedByStatus.length != 0) {
        Swal.fire('Success', `${tasksOrderedByStatus.length} tasks found`, 'success').then(() => {
          this.service.loadTasks(tasksOrderedByStatus.reverse());
        });
      } else {
        Swal.fire('Message', 'No tasks found', 'warning');
      }
    }
  }

  filterByName(id) {
    if (document.getElementById(id).value.length != 0) {
      if (document.querySelectorAll('.error').length == 0) {
        let tasksOrderedByName = this.service.filterByName(id);

        if (tasksOrderedByName.length != 0) {
          Swal.fire('Success', `${tasksOrderedByName.length} tasks found`, 'success').then(() => {
            this.service.loadTasks(tasksOrderedByName.reverse());
          });
        } else {
          Swal.fire('Message', 'No tasks found', 'warning');
        }
      }
    }
  }

  sortByDate(id) {
    let icon = document.getElementById(id);
    const tasks = this.storage.getItem('tasks');

    if (icon.classList.contains('bi-caret-down-fill')) {
      icon.classList.remove('bi-caret-down-fill');
      icon.classList.add('bi-caret-up-fill');

      this.service.loadTasks(tasks);
    } else {
      icon.classList.remove('bi-caret-up-fill');
      icon.classList.add('bi-caret-down-fill');

      this.service.loadTasks(tasks.reverse());
    }
  }

  checkCheckboxValue(checkboxGroup) {
    const inputs = document.querySelectorAll(`#${checkboxGroup} > input[type='radio']`);

    if (inputs[0].hasAttribute('checked')) {
      inputs[0].removeAttribute('checked');
      inputs[1].setAttribute('checked', 'true');
    } else {
      inputs[1].removeAttribute('checked');
      inputs[0].setAttribute('checked', 'true');
    }
  }
}
