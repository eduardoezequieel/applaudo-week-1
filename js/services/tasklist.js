import LocalStorage from '../helpers/storage.js';
import Task from '../models/task.js';

export default class TaskList {
  storage = new LocalStorage();

  addTask() {
    let tasks = this.storage.getItem('tasks');
    if (tasks.length == 0) {
      tasks = [];
    }

    let newTask = new Task(document.querySelector('#txtName').value, document.querySelector('#cbAssignee').value, 'Pending');
    tasks.push(newTask);

    this.storage.saveItem('tasks', tasks);

    return newTask.id;
  }

  deleteTask(id) {
    this.storage.deleteItem('tasks', id);
  }

  loadTasks(data) {
    let rows = '';

    if (data != null) {
      data.map(function (row) {
        rows += `
          <tr class="animate__animated animate__fadeIn" id="row-${row.id}">
              <td><div class="mt-1">${row.id}</div></td>
              <td><div class="mt-1">${row.name}</div></td>
              <td><div class="mt-1">${row.assignee}</div></td>
              <td><div class="mt-1">${row.creationDate}</div></td>
              <td>
                  <div class="my-0">
                      <button 
                      id="${row.id}"
                      class="${row.status == 'Pending' ? 'pending-status' : 'done-status'} 
                      btn-sm px-3">${row.status}
                      </button>
                  </div>
              </td>
              <td>
                  <div class="my-0">
                      <button 
                      id=${row.id}
                      data-bs-toggle="modal" 
                      data-bs-target="#editTask-modal" 
                      class="m-0 btn-sm btn btn-table">
                        <i id=${row.id} class="bi bi-pencil-fill"></i>
                      </button>

                      <button
                      id="${row.id}"
                      type="button"
                      class="m-0 btn-sm btn btn-table-2">
                        <i id="${row.id}" class="bi bi-trash-fill"></i>
                      </button>
                  </div>
              </td>
          </tr>
        `;
        document.getElementById('tasks-table').innerHTML = rows;
      });
    }
  }

  getTask(id) {
    let tasks = this.storage.getItem('tasks');
    let index = 0;

    for (let i = 0; i < tasks.length; i++) {
      if (tasks[i].id == id) {
        index = i;
      }
    }

    return tasks[index];
  }

  editTask() {
    let tasks = this.storage.getItem('tasks');
    let index = 0;

    let id = document.getElementById('txtId').value;
    for (let i = 0; i < tasks.length; i++) {
      if (tasks[i].id == id) {
        index = i;
      }
    }

    tasks[index].name = document.getElementById('txtName2').value;
    tasks[index].assignee = document.getElementById('cbAssignee2').value;
    tasks[index].status = document.querySelector("#checkboxGroup-1 > input[checked='true']").value;

    this.storage.saveItem('tasks', tasks);

    document.querySelector(".modal-footer > button[data-bs-dismiss='modal']").click();
  }

  deleteTask(id) {
    this.storage.deleteItem('tasks', id);
  }

  changeStatus(id) {
    let tasks = this.storage.getItem('tasks');

    let index = 0;

    for (let i = 0; i < tasks.length; i++) {
      if (tasks[i].id == id) {
        index = i;
      }
    }

    if (tasks[index].status == 'Pending') {
      tasks[index].status = 'Done';
    } else if (tasks[index].status == 'Done') {
      tasks[index].status = 'Pending';
    }

    this.storage.saveItem('tasks', tasks);

    return index;
  }

  filterByDate(id) {
    let tasks = this.storage.getItem('tasks');
    const date = new Date(document.getElementById(id).value);

    return tasks.filter((task) => {
      return task.creationDate == `${date.getDate() + 1} / ${date.getMonth() + 1} / ${date.getFullYear()}`;
    });
  }

  filterByStatus(id) {
    let tasks = this.storage.getItem('tasks');

    return tasks.filter((task) => {
      return task.status == `${document.getElementById(id).value}`;
    });
  }

  filterByName(id) {
    let tasks = this.storage.getItem('tasks');
    let input = document.getElementById(id);

    return tasks.filter((task) => {
      return task.name.toLowerCase().includes(input.value.toLowerCase());
    });
  }
}
