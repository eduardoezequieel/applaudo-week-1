import LocalStorage from '../helpers/storage.js';

export default class Task {
  storage = new LocalStorage();

  constructor(name, assignee, status) {
    this.id = this.#generateId();
    this.name = name;
    this.assignee = assignee;
    this.status = status;
    this.creationDate = this.#generateDate();
  }

  #generateId() {
    let tasks = this.storage.getItem('tasks');
    return tasks.length > 0 ? parseInt(tasks[tasks.length - 1].id) + 1 : 1;
  }

  #generateDate() {
    const date = new Date();
    return `${date.getDate()} / ${date.getMonth() + 1} / ${date.getFullYear()}`;
  }
}
